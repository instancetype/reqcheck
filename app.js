/**
 * Created by instancetype on 8/20/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  express = require('express')
, path = require('path')
, favicon = require('static-favicon')
, logger = require('morgan')
, cookieParser = require('cookie-parser')
, bodyParser = require('body-parser')
, mongoose = require('mongoose').connect('mongodb://localhost/requests')

const
  routes = require('./routes/index')

var app = express()

app
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'jade')

app
  .use(favicon())
  .use(logger('dev'))
  .use(bodyParser.json())
  .use(bodyParser.urlencoded())
  .use(cookieParser())
  .use(require('less-middleware')(path.join(__dirname, 'public')))
  .use(express.static(path.join(__dirname, 'public')))

app
  .use('/', routes)



app.use(function(req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500)
    res.render(
      'error'
    , { message : err.message
      , error   : {}
      }
    )
  })
}

app.use(function(err, req, res, next) {
  res.status(err.status || 500)
  res.render(
    'error'
  , { message : err.message
    , error   : {}
    }
  )
})


module.exports = app