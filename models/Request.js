/**
 * Created by instancetype on 8/20/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  mongoose = require('mongoose')
, requestSchema = mongoose.Schema(
    { head : mongoose.Schema.Types.Mixed
    , body : mongoose.Schema.Types.Mixed
    , url  : String
    , method : String
    , query : mongoose.Schema.Types.Mixed
    , time : Date
    }
  )

var Request = mongoose.model('Request', requestSchema)
module.exports = Request