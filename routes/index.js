var express = require('express')
  , router = express.Router()
  , Request = require('../models/Request')


router
  .get('/test/results', reqReporter)
  .get('/test', reqCreator)
  .head('/test', reqCreator)
  .put('/test', reqCreator)
  .post('/test', reqCreator)
  .delete('/test', reqCreator)


function reqCreator(req, res) {
  new Request(
    { head : req.headers
    , body : req.body
    , url  : req.url
    , method : req.method
    , query : req.query
    , time : Date.now()
    }
  ).save(redirect)

  function redirect(err) {
    if (err) console.log(err)
    res.redirect(303, '/test/results')
  }
}

function reqReporter(req, res) {
  Request.find().sort({ $natural : -1 }).limit(10).exec(function(err, requests) {
    if (err) console.log(err)
    console.log(requests)
    var results = []

    requests.forEach(function(reqData) {
      var result =
        { request : reqData.method + ' ' + reqData.url
        , headers : reqData.head
        , time    : reqData.time
        , body    : reqData.body
        }
      results.push(result)
    })
    res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
    res.setHeader('Pragma', 'no-cache')
    res.setHeader('Expires', '0')
    res.render('results', { title : 'CRUDback', requests : results })
  })
}

module.exports = router
